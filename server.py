from bottle import Bottle,  run, template, route, static_file, get, request, redirect
import os, glob, json, sensor, time


app = Bottle()
ROOT_PATH = os.path.dirname(os.path.abspath(__file__))

@app.route('/')
@app.route('/index')
def index():
    with open("%s/config.json" % ROOT_PATH) as f:
        config = json.load(f)

    dir = glob.glob('%s/input/*' % ROOT_PATH)
    files = []

    for out in dir:
        name, ext = os.path.splitext(out)
        files.append(out.split('/')[-1])

    return template('%s/templates/index.tpl'  % ROOT_PATH, config=config, files=files, root_path=ROOT_PATH)

@app.route('/calibrate')
def calibrate():
    with open("%s/config.json" % ROOT_PATH) as f:
        config = json.load(f)
    config['calibrate'] = True
    with open("%s/config.json" % ROOT_PATH, "w") as f:
        json.dump(config, f)

    waitForCalibration = 10
    while "calibrate" in config and waitForCalibration > 0:
        with open("%s/config.json" % ROOT_PATH) as f:
            config = json.load(f)
        waitForCalibration -= 1
        time.sleep(1)
    if(calibrate in config):
        logger.error("calibration doesn't seem to work")
    redirect('/')
@app.route('/change_settings', method='POST')
def change_settings():
    data = request.POST

    with open("%s/config.json" % ROOT_PATH) as f:
        config = json.load(f)

    for i in config:
        if(data.get(i) == None):
            continue
        if(data.get(i) == "true"):
            config[i] = True
        else:
            config[i] = data.get(i)

    with open("%s/config.json" % ROOT_PATH, "w") as f:
        json.dump(config, f)

    redirect('/')

@app.route('/upload_sound', method='POST')
def upload():
    data = request.files.data
    name, ext = os.path.splitext(data.filename)

    if data and data.file:
        if ext not in ('.mp3','.wav'):
            return "le fichier n'est pas un .wav .mp3"
        raw = data.file.read()
        filename = data.filename
        file = open('%s/input/%s' % (ROOT_PATH, filename), 'wb')
        file.write(raw)
        file.close()

    redirect('/')

@app.route('/remove_sound', method='POST')
def remove_sound():
    file = request.POST.get('sound-file')
    path = '%s/input/%s' % (ROOT_PATH, file)
    if os.path.isfile(path) or os.path.islink(path):
        os.remove(path)  # remove the file
    redirect('/')


run(app, host="0.0.0.0", port=8088, reloader=True, debug=True)
