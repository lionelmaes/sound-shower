# sound-shower

A python application that triggers a sound when someone is entering a zone. Running on a Raspberry PI acting as a wifi hostpot, it can be configured via a custom web interface. The sensor is a ultrasound sensor HC-SR04.