% rebase(root_path+'/templates/base.tpl')
  <h1>Sound shower configuration</h1>

  <h2>Change settings</h2>
  <form action="/change_settings" method="post">
    <label for="baseDistance">Initial distance (cm)</label>
    <input type="number" name="baseDistance" step="any" value="{{config["baseDistance"]}}"> <a href="/calibrate">re-calibrate</a>
    <p class="about">The distance between the sensor and the closest obstacle when nobody is in the zone.</p>
    <label for="calibrateAtStartup">Initial distance calibration at startup</label>
    <input type="checkbox" value="true" name="calibrateAtStartup" {{"checked" if config["calibrateAtStartup"] else ""}}>
    <p>Check if you want to measure initial distance at startup</p>
    <label for="triggerDistance">Trigger distance (cm)</label>
    <input type="number" step="any" name="triggerDistance" value="{{config["triggerDistance"]}}">
    <p>The system will trigger sound play if the difference between the closest obstacle detected and the initial distance is greater than the trigger distance.</p>
    <label for="sensorSpeed">Sensor speed (s)</label>
    <input type="number"  step="any" name="sensorSpeed" value="{{config["sensorSpeed"]}}">
    <p>The sensor calls frequency (s)</p>
    <label for="detectionDelay">Detection delay (s)</label>
    <input type="number"  step="any" name="detectionDelay" value="{{config["detectionDelay"]}}">
    <p>Delay between each detection cycle</p>
    <label for="repetitionTreshold">Repetition treshold</label>
    <input type="number" name="repetitionTreshold" value="{{config["repetitionTreshold"]}}">
    <p>Number of similar detections before considering a detection valid (avoid noise)</p>
    <label for="diffTreshold">Difference treshold (cm)</label>
    <input type="number"  step="any" name="diffTreshold" value="{{config["diffTreshold"]}}">
    <p>Maximum difference between two detections to consider them similar.</p>
    <label for="sensorTimeout">Sensor timeout (s)</label>
    <input type="number"  step="any" name="sensorTimeout" value="{{config["sensorTimeout"]}}">
    <p>Maximum time for the sensor to get valid detection.</p>
    <label for="autoStop">Auto stop</label>
    <input type="checkbox" value="true" name="autoStop" {{"checked" if config["autoStop"] else ""}}>
    <p>Check if you want to stop playback when nobody is in the detection zone.</p>
    <label for="stopCountdown">Stop countdown</label>
    <input type="number" name="stopCountdown" value="{{config["stopCountdown"]}}">
    <p>Number of detections that return false (non-presence) before stopping the sound.</p>
    <label for="loop">Loop</label>
    <input type="checkbox" value="true" name="loop" {{"checked" if config["loop"] else ""}}>
    <p>Check if you want to loop after playback (if somebody is still in the detection zone).</p>
    <label for="volume">Volume</label>
    <input type="number" name="volume"  step="any" value="{{config["volume"]}}">
    <p>Playback volume</p>
    <label for="fadeout">Fadeout time (ms)</label>
    <input type="number"  step="any" name="fadeout" value="{{config["fadeout"]}}">
    <p>Fadeout time before playback stop.</p>
    <label for="soundFile">Sound file</label>
    <select name="soundFile">
    % for f in files:
    <option value="{{f}}" {{!'selected="selected"' if config["soundFile"] == f else ""}}>{{f}}</option>
    % end
    <input type="submit">
  </select>
  </form>

  <h2>Upload new sound</h2>

	<form action="/upload_sound" method="post" enctype="multipart/form-data">
		<input type="file" name="data" />
		<input type="submit">
	</form>

  <h2>Remove sound</h2>

  <form action="/remove_sound" method="post" enctype="multipart/form-data">
    <select name="sound-file">
    % for f in files:
      % if f != config["soundFile"]:
      <option value="{{f}}">{{f}}</option>
      % end
    % end
    </select>
    <input type="submit">
  </form>
