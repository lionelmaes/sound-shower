<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<style>
	body,html {
		padding: 0;
		margin: 0;
		font-size:5pt;
	}
	main{
		margin:4rem;
		font-family:monospace;
		font-size:2rem;
	}
	input, select{
		border-top:1rem #CCC solid;
		border-left:1rem #CCC solid;
		border-right:1rem #000 solid;
		border-bottom:1rem #000 solid;
	}
	input[type="submit"]{
		display:block;
		margin-top:2rem;
	}
	label{
		display:inline-block;

		width:40rem;
	}

</style>
</head>
<body>
<main>
	{{!base}}
</main>
</body>
</html>
