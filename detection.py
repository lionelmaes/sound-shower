import sensor, json, time, signal, os, pygame
from logger import logger
import RPi.GPIO as GPIO

def handler(signum, frame):
    raise IOError("Sensor timeout")

def detectPresence():
    distance = measure()
    if(float(config["baseDistance"]) - distance > float(config["triggerDistance"])):
        return True
    else:
        return False

def measure():
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(int(float(config["sensorTimeout"])))
    valid = 1
    aValue = -1
    for i in range(0, 100):
        bValue = sensor.distance()
        logger.debug("distance = %s" % bValue)
        if(abs(aValue - bValue) > float(config["diffTreshold"])):
            valid = 1
        else:
            valid += 1
        if(valid >= int(float(config["repetitionTreshold"]))):
            return bValue
            break
        aValue = bValue
        time.sleep(float(config["sensorSpeed"]))
    return False


try:
    logger.info("starting detection script")
    ROOT_PATH = os.path.dirname(os.path.abspath(__file__))

    with open("%s/config.json" % ROOT_PATH) as f:
        config = json.load(f)

    if(config["calibrateAtStartup"] == True):
        logger.info("#start calibration")
        config["baseDistance"] = measure()
        logger.info("saving new base distance : %s cm" % config["baseDistance"])
        with open("%s/config.json" % ROOT_PATH, "w") as f:
            json.dump(config, f)

    logger.info("pygame init")
    pygame.mixer.init()
    pygame.mixer.music.load("%s/input/%s" % (ROOT_PATH, config["soundFile"]))
    pygame.mixer.music.set_volume(float(config["volume"]))

    oldState = False
    stopCountdown = int(float(config["stopCountdown"]))
    rewinded = True

    while True:
        with open("%s/config.json" % ROOT_PATH) as f:
            config = json.load(f)
        if "calibrate" in config:
            config["baseDistance"] = measure()
            logger.info("saving new base distance : %s cm" % config["baseDistance"])
            del config["calibrate"]
            with open("%s/config.json" % ROOT_PATH, "w") as f:
                json.dump(config, f)

        currentState = detectPresence()


        logger.debug("presence detection: %s " % currentState)

        isPlaying = pygame.mixer.music.get_busy()

        #somebody entered the zone
        if(oldState == False and currentState == True):
            if(isPlaying == False):
                logger.info("somebody entered: sound play")
                pygame.mixer.music.rewind()
                pygame.mixer.music.set_volume(float(config["volume"]))
                pygame.mixer.music.play()
                rewinded = False
            oldState = True
            stopCountdown = int(float(config["stopCountdown"]))

        #somebody left the zone
        elif(oldState == True and currentState == False):
            if(stopCountdown > 0):
                logger.info("somebody left: countdown before sound stop: %s " % stopCountdown)
                stopCountdown -= 1
            else:
                oldState = False
                if(isPlaying and config["autoStop"]):
                    stopCountdown = int(float(config["stopCountdown"]))
                    logger.info("sound fading out")
                    pygame.mixer.music.fadeout(int(float(config["fadeout"])))

        #somebody is staying in the zone
        elif(oldState == True and currentState == True):
            stopCountdown = int(float(config["stopCountdown"]))
            if(isPlaying == False and config["loop"] == True):
                logger.info("sound loop")
                pygame.mixer.music.play()
                rewinded = False
        #nobody is in the zone
        else:
            if(isPlaying == False and rewinded == False):
                logger.info("sound rewind")
                rewinded = True
                pygame.mixer.music.load("%s/input/%s" % (ROOT_PATH, config["soundFile"]))

        time.sleep(float(config["detectionDelay"]))


except IOError as e:
    logger.error(e)
except KeyboardInterrupt:
    print("Keyboard interrupt.")
    GPIO.cleanup()


GPIO.cleanup()
