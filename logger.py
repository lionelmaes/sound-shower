#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from logging.handlers import RotatingFileHandler


logger = logging.getLogger()
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')


fileHandler = RotatingFileHandler("/home/pi/sound-shower/logs/output.log", 'a', 100000, 1)


fileHandler.setLevel(logging.INFO)
fileHandler.setFormatter(formatter)
logger.addHandler(fileHandler)

'''
steamHandler = logging.StreamHandler()
steamHandler.setLevel(logging.DEBUG)
logger.addHandler(steamHandler)
'''
